# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Seong-ho Cho <darkcircle.0426@gmail.com>, 2011
# Seong-ho Cho <darkcircle.0426@gmail.com>, 2011,2013
# Shinjo Park <kde@peremen.name>, 2015
# Seong-ho Cho <darkcircle.0426@gmail.com>, 2011,2013
# Shinjo Park <kde@peremen.name>, 2015,2018
msgid ""
msgstr ""
"Project-Id-Version: accounts service\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-01-17 09:48-0500\n"
"PO-Revision-Date: 2019-02-22 14:19+0000\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean (http://www.transifex.com/freedesktop/accountsservice/language/ko/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ko\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../data/org.freedesktop.accounts.policy.in.h:1
msgid "Change your own user data"
msgstr "개인 사용자 데이터 변경"

#: ../data/org.freedesktop.accounts.policy.in.h:2
msgid "Authentication is required to change your own user data"
msgstr "내 사용자 데이터를 변경하려면 인증해야 합니다"

#: ../data/org.freedesktop.accounts.policy.in.h:3
msgid "Manage user accounts"
msgstr "사용자 계정 관리"

#: ../data/org.freedesktop.accounts.policy.in.h:4
msgid "Authentication is required to change user data"
msgstr "사용자 데이터를 변경하려면 인증해야 합니다"

#: ../data/org.freedesktop.accounts.policy.in.h:5
msgid "Change the login screen configuration"
msgstr "로그인 화면 환경 설정 변경"

#: ../data/org.freedesktop.accounts.policy.in.h:6
msgid "Authentication is required to change the login screen configuration"
msgstr "로그인 화면 환경 설정을 변경하려면 인증해야 합니다"

#: ../src/main.c:127
msgid "Output version information and exit"
msgstr "버전 정보를 출력하고 끝내기"

#: ../src/main.c:128
msgid "Replace existing instance"
msgstr "기존 인스턴스 대체"

#: ../src/main.c:129
msgid "Enable debugging code"
msgstr "디버깅 모드 활성화"

#: ../src/main.c:149
msgid ""
"Provides D-Bus interfaces for querying and manipulating\n"
"user account information."
msgstr "사용자 계정 정보를 요청하고 처리하기 위한 D-Bus 인터페이스를\n제공합니다."
